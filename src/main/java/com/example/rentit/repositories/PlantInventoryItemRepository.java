package com.example.rentit.repositories;

import com.example.rentit.models.PlantInventoryEntry;
import com.example.rentit.models.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {
    //PlantInventoryItem findOneByPlantInfo(PlantInventoryEntry entry);

    @Query("select p, count(p) from PlantInventoryItem p where p.plantInfo.name = ?1 and" +
            " p.equipmentCondition = com.example.inventory.domain.model.EquipmentCondition.SERVICEABLE" +
            " and p not in (select r.plantId from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)")
     PlantInventoryItem findAvailItems(String name, LocalDate startDate, LocalDate endDate);


//    List<PlantInventoryItem> notMaintainForAYear();

}