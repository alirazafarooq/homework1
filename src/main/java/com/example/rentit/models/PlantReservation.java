package com.example.rentit.models;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
public class PlantReservation {

    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    PlantInventoryEntry plantId;

    @ManyToOne
    PurchaseOrder purchase;

    @OneToOne
    PlantInventoryItem plantInventoryItem;

    @Embedded
    BusinessPeriod rentalPeriod;
}
