package com.example.rentit.models;

public enum EquipmentCondition {
    SERVICEABLE, UNSERVICEABLEREPAIRABLE,
    UNSERVICEABLECONDEMNED,UNSERVICEABLEINCOMPLETE
}
