package com.example.rentit.models;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class MaintenancePlan {

 @Id @GeneratedValue
 Long id;

 Long year_of_action;

 @ManyToOne
 PlantInventoryItem plantInventoryItem;

 @OneToMany(cascade = {CascadeType.ALL})
 List<MaintenanceTask> maintenanceTask;

}
