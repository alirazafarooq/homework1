package com.example.rentit.models;



public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED
}
