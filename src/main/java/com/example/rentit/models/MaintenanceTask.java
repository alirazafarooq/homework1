package com.example.rentit.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class MaintenanceTask {
    @Id
    @GeneratedValue
    Long id;

    String description;

    @Column(precision=8, scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    TypeOfWork type_of_work;

    @Embedded
    BusinessPeriod rentalPeriod;

    @ManyToOne
    PlantReservation reservation;
}
