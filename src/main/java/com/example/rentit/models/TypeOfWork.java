package com.example.rentit.models;

public enum TypeOfWork {
    PREVENTIVE,CORRECTIVE,OPERATIVE
}
