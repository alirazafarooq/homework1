package com.example.rentit.models;


import lombok.Data;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class PurchaseOrder {
    @Id @GeneratedValue
    Long id;

   @OneToMany
   List<PlantReservation> reservations;

    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;
    LocalDate paymentSchedule;
    @Column(precision=8, scale=2)
    BigDecimal total;


    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

//    @Repository
//    public static interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long> {
//        List<PlantInventoryEntry> findByNameContaining(String str);
//        @org.springframework.data.jpa.repository.Query("select p from PlantInventoryEntry p where LOWER(p.name) like ?1")
//        List<PlantInventoryEntry> finderMethod(String name);
//        @Query(value="select * from plant_inventory_entry where LOWER(name) like ?1", nativeQuery=true)
//        List<PlantInventoryEntry> finderMethodV2(String name);
//    }
}
