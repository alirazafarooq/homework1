package com.example.rentit.repositories;

import com.example.rentit.RentitApplication;
import com.example.rentit.models.PlantInventoryEntry;
import com.example.rentit.models.PlantInventoryItem;
import com.example.rentit.models.PurchaseOrder;
import com.example.rentit.repositories.PlantReservationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
@Sql(scripts="/plants-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InventoryRepositoryTest {
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;
   // @Autowired
    //PlantReservationRepository plantReservationRepo;
    @Autowired
    InventoryRepository inventoryRepo;

    @Test
    public void queryPlantCatalog() {
        assertThat(plantInventoryEntryRepo.count()).isEqualTo(14l);
    }

    @Test
    public void queryByName() {
        List<PlantInventoryEntry> x = plantInventoryEntryRepo.findByNameContaining("Mini");
        System.out.println(x.get(0));

        assertThat(plantInventoryEntryRepo.findByNameContaining("Mini").size()).isEqualTo(2);
    }

   @Test
   public void findAvailableTest() {
//       PlantInventoryEntry entry = plantInventoryEntryRepo.findOne(1l);
//       System.out.println(entry);
//       PlantInventoryItem item = plantInventoryItemRepo.findOneByPlantInfo(entry);
//       System.out.println(item);

        //assertThat(inventoryRepo.findAvailablePlants(item.getPlantInfo().getName(),LocalDate.of(2017,2,20), LocalDate.of(2017,2,25)))
          //      .contains(entry);
//
//        PlantReservation po = new PlantReservation();
//        po.setPlant(item);
//        po.setSchedule(BusinessPeriod.of(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 2, 25)));
//        plantReservationRepo.save(po);
//
//        assertThat(inventoryRepo.findAvailablePlants(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25)))
//                .doesNotContain(entry);
    }
//@SpringBootApplication
//@EnableTransactionManagement
//@ComponentScan({"com.example.rentit.repositories"})
//static class TestConfiguration{}
}

